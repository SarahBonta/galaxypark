$("#toggle").on("click", function(){
    var x = $("#navigation")[0];
    if (x.style.display === "none" || x.style.display === "") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
});

$(window).on("resize", function(){
    if(window.innerWidth >= 761) {
        $("#navigation").css("display", "block");
    } else {
        $("#navigation").css("display", "none");
    }
});